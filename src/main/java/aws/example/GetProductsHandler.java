package aws.example;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GetProductsHandler implements RequestHandler<ProductRequest, ProductResponse> {
    private static final String DB_TITLE = "products";
    private static final String SUCCESS_MESSAGE = "Retrieved Successfully!!!";
    private final Regions REGION = Regions.US_EAST_1;

    @Override
    public ProductResponse handleRequest(ProductRequest productRequest, Context context) {
        AmazonDynamoDBClient client = initDynamoDbClient();
        ScanRequest scanRequest = new ScanRequest().withTableName(DB_TITLE);
        List<Map<String, AttributeValue>> retrievedItems;
        try {
            ScanResult result = client.scan(scanRequest);
            retrievedItems = new ArrayList<>(result.getItems());
        } catch (Throwable e) {
            throw new UnableToSaveItemException("Unable to get items: ", e);
        }
        return ProductResponse.builder()
                .message(SUCCESS_MESSAGE)
                .products(retrievedItems)
                .build();
    }

    private AmazonDynamoDBClient initDynamoDbClient() {
        AmazonDynamoDBClient client = new AmazonDynamoDBClient();
        client.setRegion(Region.getRegion(REGION));
        return client;
    }
}
